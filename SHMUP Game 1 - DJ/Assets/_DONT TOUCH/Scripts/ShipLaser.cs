﻿using UnityEngine;
using System.Collections;
using System;

public class ShipLaser : MonoBehaviour
{
	static int CollLayerMask = (1 << Alias.LAYER_ENEMIES) | (1 << Alias.LAYER_MAP);

	[SerializeField]
	Transform mapHitObject;

	Transform beamOrigin;
	Vector2 endPoint;
	Vector2 defaultScale;
	float dmgPerSec;
	float maxLength;


	void Awake()
	{
		defaultScale = transform.localScale;
	}


	public void init(Transform origin, float dmg)
	{
		beamOrigin = origin;
		dmgPerSec = dmg;

		initHitObject();
	}


	void initHitObject()
	{
		if (mapHitObject != null)
		{
			mapHitObject.parent = null;
			mapHitObject.localScale = Vector3.one;
		}
	}


	void OnEnable()
	{
		PlayerShipShoot.OnStopLaser += handleOnStopLaser;
	}


	void OnDisable()
	{
		PlayerShipShoot.OnStopLaser -= handleOnStopLaser;
	}


	private void handleOnStopBullet()
	{
		throw new NotImplementedException();
	}


	private void handleOnStartBullet()
	{
		throw new NotImplementedException();
	}


	void Update()
	{
		move();
		checkColl();
		updateVisual();
	}


	private void handleOnStopLaser()
	{
		endMyself();
	}


	private void move()
	{
		transform.position = beamOrigin.position;
		transform.rotation = beamOrigin.rotation;
	}


	private void checkColl()
	{
		//Find intersection bw laser point and screen edge
		Vector2 endPtScrnEdge;
		float x, y;

		Vector2 p = beamOrigin.position; //known pt on the line
		Vector2 fv = beamOrigin.right; //slope in parametric format

		//finding x edge
		if (fv.x == 0)
			fv.x = 0.0001f;

		//Special case: horizontal line
		if (fv.y == 0f) 
		{
			x = fv.x > 0f ? CamScroller.Me.screenBoundaries.xMax : CamScroller.Me.screenBoundaries.xMin;
			endPtScrnEdge = new Vector2(x, p.y);
		}
		//Special case: vertical line
		else if (fv.x == 0f)
		{
			y = fv.y > 0f ? CamScroller.Me.screenBoundaries.yMax : CamScroller.Me.screenBoundaries.yMin;
			endPtScrnEdge = new Vector2(p.x, y);
		}
		//Other straight line, requires to find line's equation
		else
		{
			float a = fv.y / fv.x; //slope
			float b = p.y - a * p.x; //offset

			x = fv.x > 0f ? CamScroller.Me.screenBoundaries.xMax : CamScroller.Me.screenBoundaries.xMin;
			y = a * x + b;
			//if the line exits the screen from top or bottom edge instead of left or right, we use yMax or yMin to find x
			if (y > CamScroller.Me.screenBoundaries.yMax || y < CamScroller.Me.screenBoundaries.yMin)
			{
				y = fv.y > 0f ? CamScroller.Me.screenBoundaries.yMax : CamScroller.Me.screenBoundaries.yMin;
				x = (y - b) / a;
			}
			endPtScrnEdge = new Vector2(x, y);
		}

		//Cast the ray
		RaycastHit2D[] hits = Physics2D.LinecastAll(beamOrigin.position, endPtScrnEdge, CollLayerMask);
		//Check for pts of contact with enemy ships or map
		for (int i = 0; i < hits.Length; i++)
		{
			//laser damages enemy ships and pass through them
			if (hits[i].collider.gameObject.layer == Alias.LAYER_ENEMIES)
			{
				hits[i].transform.GetComponent<EnemyCollider>().onHitByLaser(dmgPerSec * Time.deltaTime);
			}
			//laser is blocked by map
			else if (hits[i].collider.gameObject.layer == Alias.LAYER_MAP)
			{
				endPoint = hits[i].point;
				updateHitObject(true);
				return;
			}
		}
		//executed only if no map collided
		endPoint = endPtScrnEdge;
		updateHitObject(false);
	}


	void updateVisual()
	{
		//Scale beam according to endPoint
		float length = (endPoint - (Vector2)beamOrigin.position).magnitude;
		transform.localScale = new Vector3(defaultScale.x * length, defaultScale.y, 1f);
	}


	void updateHitObject(bool isDisplayed)
	{
		if (mapHitObject != null)
		{
			if (isDisplayed)
			{
				if (!mapHitObject.gameObject.activeSelf)
					mapHitObject.gameObject.SetActive(true);
				mapHitObject.position = endPoint;
			}
			else
			{
				if (mapHitObject.gameObject.activeSelf)
					mapHitObject.gameObject.SetActive(false);
			}
		}
	}


	private void endMyself()
	{
		endHitObject();
		Destroy(gameObject);
	}


	void endHitObject()
	{
		if (mapHitObject != null)
		{
			Destroy(mapHitObject.gameObject);
		}
	}

}
