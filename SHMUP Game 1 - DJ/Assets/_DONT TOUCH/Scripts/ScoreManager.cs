﻿using System;
using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
	[Serializable]
	public struct EnemyValue
	{
		public Enemies enemy;
		public int value;
	}

	[SerializeField]
	EnemyValue[] enemyValues;

	int score;

	static public event DelegateOneInt OnScoreUpdated;


	// Use this for initialization
	void Start()
	{
		score = 0;
		setScore(score);
	}


	void OnEnable()
	{
		EnemyHealth.OnKilledStatic += handleOnEnemyKilled;
	}


	void OnDisable()
	{
		EnemyHealth.OnKilledStatic -= handleOnEnemyKilled;
	}


	private void handleOnEnemyKilled(Enemies e)
	{
		int addedVal = -1;

		foreach (EnemyValue ev in enemyValues)
		{
			if (ev.enemy == e)
			{
				addedVal = ev.value;
				break;
			}
		}

		if (addedVal > -1)
		{
			addScore(addedVal);
		}
		else
			Debug.Log("BUG in Scorer: enemy type not referenced");//DEBUG
	}


	void addScore(int addedVal)
	{
		setScore(score + addedVal);
	}


	void setScore(int newVal)
	{
		score = newVal;

		if (OnScoreUpdated != null)
			OnScoreUpdated(score);
	}

}
