﻿using UnityEngine;
using System.Collections;

public abstract class TriggerableObject : MonoBehaviour
{
	public void triggerMe()
	{
		enabled = true;
	}
}
