﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class GameManager : MonoBehaviour
{
	static GameManager Me;

	//[SerializeField]
	//float initDuration = 1f;
	[SerializeField]
	float gameOverDuration = 2f;
	[SerializeField]
	float victoryDuration = 0f;
	[SerializeField]
	int nextSceneIndex;

	GameStates state;


	public static event System.Action OnPlayStart;
	public static event System.Action OnGameOver;
	public static event System.Action OnVictory;


	static public void GoVictory()
	{
		Me.state = GameStates.victory;
	}


	void Awake()
	{
		Me = this;
		StartCoroutine(coInit());
	}


	void OnEnable()
	{
		PlayerShipHealth.OnShipDestroyed += handleOnShipDestroyed;
		//LevelEnd.OnReachedExit += handleOnReachedExit;
	}


	void OnDisable()
	{
		PlayerShipHealth.OnShipDestroyed -= handleOnShipDestroyed;
		//LevelEnd.OnReachedExit -= handleOnReachedExit;
	}


	private void handleOnShipDestroyed()
	{
		state = GameStates.over;
	}


	//private void handleOnReachedExit()
	//{
	//	state = GameStates.victory;
	//}


	IEnumerator coInit()
	{
		state = GameStates.init;

		//yield return new WaitForSeconds(initDuration);
		yield return StartCoroutine(ScreenFader.Me.coFade(ScreenFader.Transitions.intro));

		if (OnPlayStart != null)
		{
			OnPlayStart();
		}

		StartCoroutine(coPlay());
	}


	IEnumerator coPlay()
	{
		state = GameStates.play;

		do
		{
			yield return null;
		} while (state == GameStates.play);

		if (state == GameStates.over)
		{
			StartCoroutine(coGameOver());
		}
		else if (state == GameStates.victory)
		{
			StartCoroutine(coVictory());
		}
	}


	IEnumerator coGameOver()
	{
		if (OnGameOver != null)
			OnGameOver();

		yield return new WaitForSeconds(gameOverDuration);

		yield return StartCoroutine(ScreenFader.Me.coFade(ScreenFader.Transitions.gameOver));

		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); //reload current scene
	}


	IEnumerator coVictory()
	{
		if (OnVictory != null)
			OnVictory();

		yield return new WaitForSeconds(victoryDuration);

		yield return StartCoroutine(ScreenFader.Me.coFade(ScreenFader.Transitions.victory));

		string sceneName;
		if (nextSceneIndex < 0)
		{
			sceneName = "Scene Level Select";
		}
		else if (nextSceneIndex < 9)
		{
			sceneName = "Scene Level " + nextSceneIndex; //"T" + LevelSelectManager.TeamLetter + " Scene Level " + nextSceneIndex;
		}
		else
		{
			sceneName = "Scene End Game";
		}
		SceneManager.LoadScene(sceneName);
	}
}


public enum GameStates
{
	init,
	play,
	over,
	victory
}

