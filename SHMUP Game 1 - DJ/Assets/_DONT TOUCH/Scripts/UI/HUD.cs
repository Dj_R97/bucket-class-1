﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUD : MonoBehaviour
{
	[SerializeField]
	Text scoreDisplay;
	[SerializeField]
	Slider hpSlider;


	void OnEnable()
	{
		ScoreManager.OnScoreUpdated += handleOnScoreUpdated;
		PlayerShipHealth.OnHpChange += handleOnShipHpChange;
	}


	void OnDisable()
	{
		ScoreManager.OnScoreUpdated -= handleOnScoreUpdated;
		PlayerShipHealth.OnHpChange -= handleOnShipHpChange;
	}


	private void handleOnShipHpChange(float hp, float max)
	{
		hpSlider.value = Mathf.InverseLerp(0f, max, hp);
	}


	private void handleOnScoreUpdated(int i)
	{
		updateScoreDisplay(i);
	}


	void updateScoreDisplay(int newVal)
	{
		scoreDisplay.text = newVal.ToString();
	}

}
