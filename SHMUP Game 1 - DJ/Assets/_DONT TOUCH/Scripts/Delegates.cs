﻿using UnityEngine;

public delegate void DelegateOneFloat(float f);
public delegate void DelegateTwoFloats(float f1, float f2);
public delegate void DelegateOneInt(int i);
//public delegate void DelegateColliderFloat(Collider2D c, float f);
