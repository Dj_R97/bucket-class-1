﻿using UnityEngine;
using System.Collections;

public abstract class TriggeringObject : TriggerableObject
{
	[SerializeField]
	protected TriggerableObject[] componentsToTrigger;
	bool mustDisableEventually = true;


	void Awake()
	{
		//disable all listed objects at level init
		foreach (TriggerableObject obj in componentsToTrigger)
		{
			if (obj != null)
				obj.enabled = false;
		}
	}


	protected void triggerOthersAndFinish()
	{
		foreach (TriggerableObject obj in componentsToTrigger)
		{
			if (obj != null)
				obj.triggerMe();
		}

		if (mustDisableEventually)
			enabled = false;
	}


	protected void mayDisable()
	{
		if (mustDisableEventually)
			enabled = false;
	}
}
