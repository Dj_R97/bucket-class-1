﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerShipHealth : MonoBehaviour {

	[SerializeField]
	Transform explosionVfxRef;
	[SerializeField]
	float maxHitPoints = 100f;
	[SerializeField]
	float dmgTakenOnMapHit = 2f;
	[SerializeField]
	float hpGainedOnPickup = 20f;

	float hp;

	static public event System.Action OnShipDestroyed;
	static public event DelegateTwoFloats OnHpChange;


	void Awake()
	{
		hp = maxHitPoints;
	}


	void OnEnable()
	{
		EnemyCollider.OnHitPlayerShip += handleOnHitByEnemy;
		EnemyBullet.OnHitPlayerShip += handleOnHitByBullet;
		Explosion.OnHitPlayerShip += handleOnHitByBullet;
		GameManager.OnVictory += handleOnVictory;
		Pickup.OnPickup += handleOnPickup;

	}


	void OnDisable()
	{
		EnemyCollider.OnHitPlayerShip -= handleOnHitByEnemy;
		EnemyBullet.OnHitPlayerShip -= handleOnHitByBullet;
		Explosion.OnHitPlayerShip -= handleOnHitByBullet;
		GameManager.OnVictory -= handleOnVictory;
		Pickup.OnPickup -= handleOnPickup;

	}


	private void handleOnHitByEnemy(Enemies e, float dmg)
	{
		takeDamage(dmg);
	}

	private void handleOnHitByBullet(float dmg)
	{
		takeDamage(dmg);
	}


	private void handleOnVictory()
	{
		enabled = false;
	}


	void handleOnPickup(Pickups type)
	{
		if (type == Pickups.Health)
		{
			if (hp == maxHitPoints)
				return;

			hp += hpGainedOnPickup;
			if (hp > maxHitPoints)
				hp = maxHitPoints;

			if (OnHpChange != null)
				OnHpChange(hp, maxHitPoints);
		}
	}


	public void gotMapHit()
	{
		takeDamage(dmgTakenOnMapHit * Time.fixedDeltaTime);
	}


	void takeDamage(float dmg)
	{
		if (dmg == 0)
			return;

		hp -= dmg;

		if (OnHpChange != null)
			OnHpChange(hp, maxHitPoints);

		if (hp <= 0)
		{
			if (OnShipDestroyed != null)
				OnShipDestroyed();
			explode();
		}
	}


	void explode()
	{
		//instantiate an explosion vfx object
		Instantiate(explosionVfxRef, transform.position, Quaternion.identity);

		//destroy this whole gameobject (including the particle system component)
		Destroy(gameObject);
	}

}
