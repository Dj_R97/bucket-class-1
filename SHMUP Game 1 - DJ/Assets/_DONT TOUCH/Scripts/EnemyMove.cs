﻿using System;
using UnityEngine;

abstract public class EnemyMove : TriggeringObject
{
	protected Rigidbody2D rb; //rigidbody2d reference
	public float speed = 5f; //speed of enemy
	[SerializeField]
	protected BehaviorChangeConditions endCondition;
	[SerializeField]
	protected float value;
	[SerializeField]
	protected EnemyHealth healthManager;

	protected Vector2 initialPos;
	private float maxTimeOffscreen = 3f;
	float offscreenTime;
	float startTime;
	protected bool hasNextMoveBehavior = false;


	void Awake()
	{
		rb = GetComponent<Rigidbody2D>();

		int enemyMoveScripts = 0;
		foreach (TriggerableObject obj in componentsToTrigger)
		{
			if (obj != null && obj.GetComponent<EnemyMove>() != null)
			{
				enemyMoveScripts++;
			}
			if (enemyMoveScripts > 1)
			{
				Debug.LogWarningFormat("<color=brown>Component " + this + " has " + enemyMoveScripts + " EnemyMove scripts in its list of components to trigger. (Should have only one.)</color>");//DEBUG
				hasNextMoveBehavior = true;
			}
		}
	}


	void Start()
	{
		doOnStart();
	}


	//USE THAT METHOD INSTEAD OF Start() FOR CLASSES THAT WILL DERIVE FROM EnemyMove!
	virtual protected void doOnStart() { }


	void OnEnable()
	{
		initialPos = transform.position;
		startTime = Time.time;

		doOnEnable();
	}


	//USE THAT METHOD INSTEAD OF OnEnable() FOR CLASSES THAT WILL DERIVE FROM EnemyMove!
	virtual protected void doOnEnable() { }


	void Update()
	{
		doOnUpdate();

		checkChangeBehaviorCondition();

		checkOffscreen();
	}


	private void checkChangeBehaviorCondition()
	{
		switch (endCondition)
		{
			case BehaviorChangeConditions.delay:
				checkDelayCondition();
				break;
			case BehaviorChangeConditions.healthRemaining:
				checkHealthCondition();
				break;
			case BehaviorChangeConditions.distance:
				checkDistanceCondition();
				break;
		}
	}


	virtual protected void checkHealthCondition()
	{
		//if hitpoints are lower (in proportion) than a certain value (btw 1 and 0), we change behavior
		if (healthManager.getHps() <= value)
		{
			triggerOthersAndFinish();
		}
	}


	virtual protected void checkDistanceCondition() { }


	virtual protected void checkDelayCondition()
	{
		if (Time.time >= startTime + value)
		{
			triggerOthersAndFinish();
		}
	}


	//USE THAT METHOD INSTEAD OF Update() FOR CLASSES THAT WILL DERIVE FROM EnemyMove!
	virtual protected void doOnUpdate() { }


	//Checks whether this enemy is on- or offscreen, as after some time offscreen the enemy is destroyed
	protected void checkOffscreen()
	{
		if (rb.position.x < CamScroller.Me.screenBoundaries.xMin
			|| rb.position.y > CamScroller.Me.screenBoundaries.yMax || rb.position.y < CamScroller.Me.screenBoundaries.yMin)
		{
			offscreenTime += Time.fixedDeltaTime;

			if (offscreenTime > maxTimeOffscreen)
			{
				justDestroy();
			}
		}
		else
		{
			offscreenTime = 0f;
		}
	}


	private void justDestroy()
	{
		Destroy(gameObject);
	}

}


public enum BehaviorChangeConditions
{
	NA,
	distance,
	healthRemaining,
	delay
}