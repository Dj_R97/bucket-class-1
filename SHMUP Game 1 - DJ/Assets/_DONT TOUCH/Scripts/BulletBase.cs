﻿using UnityEngine;

public abstract class BulletBase : MonoBehaviour
{
	[SerializeField]
	protected Rigidbody2D rb;
	[SerializeField]
	protected float defaultDamage = 5f;
	[SerializeField]
	protected float defaultSpeed = 8f;
	[SerializeField]
	protected int maxBounce = 0;
	//[SerializeField]
	//protected float sineAmp = 0f;
	//[SerializeField]
	//protected float sineSpeed = 1f;
	[SerializeField]
	protected Timer doomTimer;
	[SerializeField]
	protected Transform toInstantiateOnMapHit;
	[SerializeField]
	protected Transform toInstantiateOnDeath;
	[SerializeField]
	protected bool keepAsChild = false;
	//float startTime;
	//float currTime;
	//float initialAngle;
	//Vector2 initialDirVector;

	protected int bounces;


	private void Awake()
	{
		if (!keepAsChild)
			transform.parent = null;
	}


	void Start()
	{
		//registering to event
		if (doomTimer != null)
			doomTimer.OnTimesUp += handleOnTimesUp;

		//startTime = Time.time;
		//currTime = 0f;
		//initialAngle = transform.eulerAngles.z;
		//initialDirVector = transform.TransformVector(Vector2.right);

		bounces = 0;

		launch();
	}


	void launch()
	{
		Vector2 localForce = transform.TransformVector(Vector3.right) * defaultSpeed;
		rb.AddForce(localForce, ForceMode2D.Impulse);
	}


	private void handleOnTimesUp()
	{
		destroyMyself();
	}


	public void onHitByExplosion()
	{
		destroyMyself();
	}


	//void Update()
	//{
	//	//Calculate forward bullet movement at this frame
	//	Vector2 move = new Vector2(speed * Time.deltaTime, 0f);
		
	//	//Add local y offset due to sine movement, if needed
	//	//if (sineAmp != 0f)
	//	//{
	//	//	currTime = Time.time;
	//	//	float a = currTime * sineSpeed;
	//	//	move.y = Mathf.Sin(a) * sineAmp;
	//	//}

	//	//Local space to global space
	//	Vector2 newPos = transform.TransformPoint(move);

	//	//Update the bullet's position
	//	transform.position = rb.position = newPos;
	//}


	void OnBecameInvisible()
	{
		destroyNClean();
	}


	protected void destroyMyself()
	{
       if (toInstantiateOnMapHit != null)
				Instantiate(toInstantiateOnDeath, transform.position, Quaternion.identity);

		destroyNClean();
	}


	void destroyNClean()
	{
		//destroy this whole gameobject
		Destroy(gameObject);
		//unregistering from event
		if (doomTimer != null)
			doomTimer.OnTimesUp -= handleOnTimesUp;
	}


	protected void hitMap()
	{
		bounces++;

		//instantiate an object
		if (bounces <= maxBounce && toInstantiateOnMapHit != null)
			Instantiate(toInstantiateOnMapHit, transform.position, Quaternion.identity);
	}


	//void OnTriggerEnter2D(Collider2D col)
	//{
	//	//detect collision of player's bullet with the map
	//	if (col.gameObject.layer == Alias.LAYER_MAP)
	//	{
	//		hitMap();
	//		if (bounces > maxBounce)
	//			destroyMyself();
	//	}
	//	//check collision of player's bullet with enemies
	//	else if (col.gameObject.layer == Alias.LAYER_ENEMIES)
	//	{
	//		col.gameObject.GetComponent<EnemyCollider>().onHitByBullet(damage);
	//		destroyMyself();
	//	}
	//}


	void OnCollisionEnter2D(Collision2D col)
	{
		//detect collision of player's bullet with the map
		if (col.gameObject.layer == Alias.LAYER_MAP)
		{
			hitMap();
			if (bounces > maxBounce)
				destroyMyself();
		}
		//check collision of player's bullet with enemies
		else {
			onCollEnterCont(col);
		}
	}


	abstract protected void onCollEnterCont(Collision2D col);

}