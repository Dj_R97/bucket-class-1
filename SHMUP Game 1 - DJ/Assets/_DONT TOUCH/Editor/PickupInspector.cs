﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Pickup))]
public class PickupInspector : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}
}
