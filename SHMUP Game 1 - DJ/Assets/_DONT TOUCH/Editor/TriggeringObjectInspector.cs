﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TriggeringObject), true)]
public class TriggeringObjectInspector : TriggerableObjectInspector
{

	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		doInOnInspectorGUI();

		serializedObject.ApplyModifiedProperties();
	}


	protected override void doInOnInspectorGUI()
	{
		displayColoredInfo();

		SerializedProperty prop = serializedObject.GetIterator();
		while (prop.NextVisible(true))
		{
			if (prop.depth == 0)
			{
				if (prop.isArray)
				{
					//Debug.Log(prop.type + " " + prop.name + " depth = " + prop.depth); //TEST
					if (prop.name.StartsWith("component"))
					{
						displayComponentsToTrigger(prop, prop.name);
					}
					else
					{
						EditorGUILayout.PropertyField(prop, true);
					}
				}
				else if (prop.type == "PPtr<$TriggerableObject>")
				{
					displayOneComponentField(prop);
				}
				else if (prop.name != "m_Script")
				{
					EditorGUILayout.PropertyField(prop, true);
				}
			}
		}
	}


	/// <summary>
	/// Displays componentsToTrigger array
	/// </summary>
	protected void displayComponentsToTrigger(SerializedProperty componentsProp, string label)
	{
		EditorGUILayout.PropertyField(componentsProp, new GUIContent(label));
		if (componentsProp.isExpanded)
		{
			EditorGUI.indentLevel += 1;
			EditorGUILayout.PropertyField(componentsProp.FindPropertyRelative("Array.size"));
			for (int i = 0; i < componentsProp.arraySize; i++)
			{
				displayOneComponentField(componentsProp.GetArrayElementAtIndex(i));
			}
			EditorGUI.indentLevel -= 1;
		}
	}


	/// <summary>
	/// Displays each field of componentsToTrigger array with the color of the component referenced
	/// </summary>
	protected void displayOneComponentField(SerializedProperty compFieldProp)
	{
		int referencedComponentId = compFieldProp.objectReferenceInstanceIDValue;
		if (referencedComponentId != 0)
		{
			int cIdx = TriggerableObjectInspector.GetComponentData(referencedComponentId).colorIndex;
			GUI.backgroundColor = CustomEditorUtils.Colors[cIdx];
		}
		//Debug.Log(GUIUtility.GetControlID(FocusType.Passive).ToString());
		EditorGUILayout.PropertyField(compFieldProp);
		GUI.backgroundColor = CustomEditorUtils.ResetColor();
	}

}
