﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(WeaponSpawnSystem))]
public class WeaponSpawnSystemInspector : Editor
{
	static int CurrColorIndex = 0;

	static List<CoupleOfInts> ComponentsData;

	GUIContent numOfLevelsContent = new GUIContent("# of upgrade levels");

	/// <summary>
	/// Returns the instance id and color index of the component which instance id is passed as parameter
	/// Note: returning instance id is useless as it must be passed as parameter, but it is part of CoupleOfInts returned struct
	/// </summary>
	static public CoupleOfInts GetComponentData(int id)
	{
		if (ComponentsData != null)
		{
			for (int j = 0; j < ComponentsData.Count; j++)
			{
				if (id == ComponentsData[j].uid)
				{
					return ComponentsData[j];
				}
			}
		}
		else
		{
			ComponentsData = new List<CoupleOfInts>();
		}

		//if this component's uid wasn't found in ComponentsData or if ComponentsData wasn't existing, we add it along with a new color
		CoupleOfInts data = new CoupleOfInts(id, GetNewColorIndex());
		ComponentsData.Add(data);

		return data;
	}


	static int GetNewColorIndex()
	{
		CurrColorIndex++;
		if (CurrColorIndex >= CustomEditorUtils.Colors.Length)
		{
			CurrColorIndex = 1;
		}

		return CurrColorIndex;
	}


	//Called each time the target gameobject's inspector becomes displayed
	void Awake()
	{
		if (ComponentsData == null)
		{
			ComponentsData = new List<CoupleOfInts>();
		}
		Object obj = serializedObject.targetObject;
		int iid = obj.GetInstanceID();
		GetComponentData(iid);
	}


	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		//Displays concerned weapon(s) info label
		displayColoredInfo();

		SerializedProperty ptsAtLvlsProp = serializedObject.FindProperty("ptsAtLvl");
		EditorGUILayout.PropertyField(ptsAtLvlsProp.FindPropertyRelative("Array.size"), numOfLevelsContent);

		for (int i = 0; i < ptsAtLvlsProp.arraySize; i++)
		{
			SerializedProperty wpsProp = ptsAtLvlsProp.GetArrayElementAtIndex(i); //one entry of the array 'ptsAtLvl'; it's an object of type WeaponSpawnPoints
			SerializedProperty pointsProp = wpsProp.FindPropertyRelative("points"); //array 'points' of the WeaponSpawnPoints property
			string str = "Spawn pts at level " + (i + 1);
			EditorGUILayout.PropertyField(pointsProp, new GUIContent(str), true);

			//if (pointsProp.isExpanded)
			//{
			//	EditorGUI.indentLevel += 3;

			//	EditorGUILayout.PropertyField(pointsProp.FindPropertyRelative("Array.size"), numPtsContent);
			//	EditorGUI.indentLevel -= 1;

			//	//Display the array fields for this level
			//	for (int j = 0; j < pointsProp.arraySize; j++)
			//	{
			//		EditorGUILayout.PropertyField(pointsProp.GetArrayElementAtIndex(j), emptyContent);
			//	}
			//	EditorGUI.indentLevel -= 2;
			//}
		}

		serializedObject.ApplyModifiedProperties();
	}


	void displayColoredInfo()
	{
		PlayerShipShoot pss = (serializedObject.targetObject as MonoBehaviour).GetComponent<PlayerShipShoot>();
		string str;
		str = "Spawn system used for: ";
		bool isUsed = false;
		for (int i = 0; i < pss.weapons.Length; i++)
		{
			WeaponSpawnSystem wss = pss.weapons[i].mySpawnSystem;
			if (wss != null && wss.GetInstanceID() == serializedObject.targetObject.GetInstanceID())
			{
				str += pss.weapons[i].type.ToString() + " ";
				isUsed = true;
			}
		}

		if (!isUsed)
		{
			str = "Weapon Spawn System not in use";
		}

		GUI.backgroundColor = CustomEditorUtils.Colors[GetComponentData(serializedObject.targetObject.GetInstanceID()).colorIndex];
		EditorGUILayout.HelpBox(str, isUsed ? MessageType.None : MessageType.Warning);
		GUI.backgroundColor = CustomEditorUtils.ResetColor();
	}

}
