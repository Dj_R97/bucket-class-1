﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TriggerableObject), true)]
public class TriggerableObjectInspector : Editor
{
	protected static int CurrColorIndex = 0;

	protected static List<CoupleOfInts> ComponentsData;

	/// <summary>
	/// Returns the instance id and color index of the component which instance id is passed as parameter
	/// Note: returning instance id is useless as it must be passed as parameter, but it is part of CoupleOfInts returned struct
	/// </summary>
	static public CoupleOfInts GetComponentData(int id)
	{
		if (ComponentsData != null)
		{
			for (int j = 0; j < ComponentsData.Count; j++)
			{
				if (id == ComponentsData[j].uid)
				{
					return ComponentsData[j];
				}
			}
		}
		else
		{
			ComponentsData = new List<CoupleOfInts>();
		}

		//if this component's uid wasn't found in ComponentsData or if ComponentsData wasn't existing, we add it along with a new color
		CoupleOfInts data = new CoupleOfInts(id, GetNewColorIndex());
		ComponentsData.Add(data);

		return data;
	}


	static int GetNewColorIndex()
	{
		CurrColorIndex++;
		if (CurrColorIndex >= CustomEditorUtils.Colors.Length)
		{
			CurrColorIndex = 1;
		}

		return CurrColorIndex;
	}


	//Called each time the target gameobject's inspector becomes displayed
	void Awake()
	{
		if (ComponentsData == null)
		{
			ComponentsData = new List<CoupleOfInts>();
		}
		Object obj = serializedObject.targetObject;
		int iid = obj.GetInstanceID();
		GetComponentData(iid);
	}


	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		doInOnInspectorGUI();

		serializedObject.ApplyModifiedProperties();
	}


	protected virtual void doInOnInspectorGUI()
	{
		//Displays concerned weapon(s) info label
		displayColoredInfo();
		DrawDefaultInspector();
	}


	protected void displayColoredInfo()
	{
		//TriggeringObject[] tObj = (serializedObject.targetObject as MonoBehaviour).GetComponents<TriggeringObject>();
		//string str;
		//str = "Used in ";
		//bool isUsed = false;
		//for (int i = 0; i < tObj.Length; i++)
		//{
		//	WeaponSpawnSystem wss = tObj.weapons[i].mySpawnSystem;
		//	if (wss != null && wss.GetInstanceID() == serializedObject.targetObject.GetInstanceID())
		//	{
		//		str += tObj.weapons[i].type.ToString() + " ";
		//		isUsed = true;
		//	}
		//}

		//if (!isUsed)
		//{
		//	str = "Weapon Spawn System not in use";
		//}

		GUI.backgroundColor = CustomEditorUtils.Colors[GetComponentData(serializedObject.targetObject.GetInstanceID()).colorIndex];
		EditorGUILayout.HelpBox("", MessageType.None);
		GUI.backgroundColor = CustomEditorUtils.ResetColor();
	}

}
