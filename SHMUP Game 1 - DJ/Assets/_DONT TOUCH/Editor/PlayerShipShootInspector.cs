﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PlayerShipShoot))]
public class PlayerShipShootInspector : Editor
{
	GUIContent weaponDataNameContent = new GUIContent("Weapons Types");
	GUIContent emptyContent = new GUIContent("");
	GUIContent dmgLevelsNameContent = new GUIContent("Damage");
	GUIContent spdLevelsNameContent = new GUIContent("Bullet Speed");
	GUIContent fireLevelsNameContent = new GUIContent("Fire Cooldown");
	GUIContent bulletToSpawnLevelsNameContent = new GUIContent("Bullet Prefab");
	GUIContent laserToSpawnLevelsNameContent = new GUIContent("Laser Prefab");
	GUIContent numOfLevelsContent = new GUIContent("# of levels");
	GUIContent spawnSystemNameContent = new GUIContent("Shoot points");


	public override void OnInspectorGUI()
	{
		serializedObject.Update();

		displayWeapons(serializedObject.FindProperty("weapons"));

		serializedObject.ApplyModifiedProperties();
	}


	private void displayWeapons(SerializedProperty weaponsList)
	{
		//Display the number of entries in 'weapons', preceded by a custom title as a label of the property
		EditorGUILayout.PropertyField(weaponsList.FindPropertyRelative("Array.size"), weaponDataNameContent);

		EditorGUI.indentLevel += 1;

		//Display each WeaponData entry
		for (int i = 0; i < weaponsList.arraySize; i++)
		{
			//Get one WeaponData entry as a SerializedProperty
			SerializedProperty weaponProp = weaponsList.GetArrayElementAtIndex(i);
			//Get the value of the 'type' field
			SerializedProperty typeProp = weaponProp.FindPropertyRelative("type");

			//Display the header/title of that weapon using the 'type' field value
			string title = typeProp.enumDisplayNames[typeProp.enumValueIndex];
			EditorGUILayout.PropertyField(weaponProp, new GUIContent(title));

			if (weaponProp.isExpanded) //enabling folding
			{
				//Display the 'type' field of the WeaponData entry without a label
				EditorGUILayout.PropertyField(typeProp, emptyContent);

				EditorGUI.indentLevel += 1;

				//Display the WeaponSpawnSystem field with the color of the component referenced
				displayWeapSystemField(weaponProp.FindPropertyRelative("mySpawnSystem"));

				//Display editable arrays of the WeaponData object
				displayLevels(weaponProp.FindPropertyRelative("damageAtLvl"), dmgLevelsNameContent);
				displayLevels(weaponProp.FindPropertyRelative("speedAtLvl"), spdLevelsNameContent);
				displayLevels(weaponProp.FindPropertyRelative("fireDelayAtLvl"), fireLevelsNameContent);

				//Display the bullet or laser prefab ref field depending on the type of weapon (above a certain enum index, it's a laser)
				if (typeProp.enumValueIndex <= 2)
				{
					displayLevels(weaponProp.FindPropertyRelative("toSpawnAtLevel"), bulletToSpawnLevelsNameContent);
				}
				else
				{
					displayLevels(weaponProp.FindPropertyRelative("laserToSpawnAtLevel"), laserToSpawnLevelsNameContent);
				}

				EditorGUI.indentLevel -= 1;
			}
		}

		EditorGUI.indentLevel -= 1;
	}


	void displayLevels(SerializedProperty list, GUIContent nameContent)
	{
		EditorGUILayout.PropertyField(list, nameContent);
		if (list.isExpanded) //enabling folding
		{
			//Display the number of level fields
			EditorGUILayout.BeginHorizontal();
			GUILayout.Space(100);
			GUILayout.Label(numOfLevelsContent);
			EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"), emptyContent);
			EditorGUILayout.EndHorizontal();

			//EditorGUI.indentLevel += 1;
			for (int j = 0; j < list.arraySize; j++)
			{
				GUIContent label = new GUIContent("at level " + (j + 1));
				EditorGUILayout.PropertyField(list.GetArrayElementAtIndex(j), label);
			}

			//EditorGUI.indentLevel -= 1;
		}
	}


	/// <summary>
	/// Displays the WeaponSpawnSystem field with the color of the component referenced
	/// </summary>
	void displayWeapSystemField(SerializedProperty wssProp)
	{
		int referencedComponentId = wssProp.objectReferenceInstanceIDValue;
		if (referencedComponentId != 0)
		{
			int cIdx = WeaponSpawnSystemInspector.GetComponentData(referencedComponentId).colorIndex;
			GUI.backgroundColor = CustomEditorUtils.Colors[cIdx];
		}
		EditorGUILayout.PropertyField(wssProp, spawnSystemNameContent);
		GUI.backgroundColor = CustomEditorUtils.ResetColor();

		//serializedObject.ApplyModifiedProperties();
	}

}
