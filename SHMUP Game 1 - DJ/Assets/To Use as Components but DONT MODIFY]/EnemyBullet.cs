﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyBullet : BulletBase
{

	static public event DelegateOneFloat OnHitPlayerShip;


	public void init(Vector3 pos, float rot)
	{
		rb.position = transform.position = pos;
		rb.rotation = rot;
		transform.eulerAngles = new Vector3(0f, 0f, rot);
	}


	//void OnTriggerEnter2D(Collider2D col)
	//{
	//	//detect collision of bullet with the map
	//	if (col.gameObject.layer == Alias.LAYER_MAP)
	//	{
	//		hitMap();
	//		if (bounces > maxBounce)
	//			destroyMyself();
	//	}
	//	//check collision of player's bullet with player ship
	//	else if (col.gameObject.layer == Alias.LAYER_SHIP)
	//	{
	//		if (OnHitPlayerShip != null)
	//			OnHitPlayerShip(damage);

	//		destroyMyself();
	//	}
	//}


	protected override void onCollEnterCont(Collision2D col)
	{
		if (col.gameObject.layer == Alias.LAYER_SHIP)
		{
			if (OnHitPlayerShip != null)
				OnHitPlayerShip(defaultDamage);

			destroyMyself();
		}
	}

}
