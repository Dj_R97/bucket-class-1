﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerOnZone : TriggerBase
{
	void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.gameObject.layer == Alias.LAYER_SHIP)
		{
			triggerOthersAndFinish();
		}
	}
}
