﻿using UnityEngine;
using System.Collections;

public class EnemyCollider : MonoBehaviour
{
	[SerializeField]
	EnemyInfo info;
	[SerializeField]
	EnemyHealth healthManager;

	public delegate void DelegateEnemyDmg(Enemies e, float dmg);
	static public event DelegateEnemyDmg OnHitPlayerShip;

	bool isMapCollidable;


	void Start()
	{
		isMapCollidable = false;
		StartCoroutine(coDelay());
	}


	IEnumerator coDelay()
	{
		yield return new WaitForSeconds(0.8f);
		isMapCollidable = true;
	}


	//Called by ShipBullet when it hits that enemy
	public void onHitByBullet(float dmg)
	{
		if (healthManager != null)
			healthManager.gotHitByWeapon(dmg);
	}


	//Called by ShipLaser when it hits that enemy
	public void onHitByLaser(float dmg)
	{
		if (healthManager != null)
			healthManager.gotHitByWeapon(dmg);
	}


	void OnCollisionEnter2D(Collision2D otherColl)
	{
		//Check if collide with player ship 
		if (otherColl.gameObject.layer == Alias.LAYER_SHIP)
		{
			if (OnHitPlayerShip != null)
				OnHitPlayerShip(info.type, info.damageOnContact);
			if (healthManager != null)
				healthManager.gotHitByShip();

		}
		//check if collide with map
		else if (otherColl.gameObject.layer == Alias.LAYER_MAP && isMapCollidable)
		{
			if (otherColl.gameObject.layer == Alias.LAYER_MAP && isMapCollidable)
				if (healthManager != null)
					healthManager.gotMapHit();
		}
	}


	void OnCollisionStay2D(Collision2D otherColl)
	{
		//Check if collide with player ship 
		if (otherColl.gameObject.layer == Alias.LAYER_SHIP)
		{
			if (OnHitPlayerShip != null)
				OnHitPlayerShip(info.type, info.damageOnContact * Time.fixedDeltaTime);
			if (healthManager != null)
				healthManager.gotHitByShip();
		}
		//check if collide with map
		else if (otherColl.gameObject.layer == Alias.LAYER_MAP && isMapCollidable)
			if (healthManager != null)
				healthManager.gotMapHit();
	}

}
