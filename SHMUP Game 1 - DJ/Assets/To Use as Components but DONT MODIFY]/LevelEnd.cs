﻿using UnityEngine;
using System.Collections;

public class LevelEnd : TriggerableObject
{
	private void OnEnable()
	{
		GameManager.GoVictory();
	}
}
