﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{
	[SerializeField]
	float amplitude;
	[SerializeField]
	float angularSpeed;
	[SerializeField]
	bool isPlayerShip = true;

	bool mustOscillate = false;
	float defaultAngle;
	float currDelta;


	void Awake()
	{
		defaultAngle = transform.eulerAngles.z;
		currDelta = 0f;
	}


	void OnEnable()
	{
		if (isPlayerShip)
		{
			PlayerShipShoot.OnStartBullet += handleOnStartFiring;
			PlayerShipShoot.OnStopBullet += handleOnStopFiring;
			PlayerShipShoot.OnStartLaser += handleOnStartFiring;
			PlayerShipShoot.OnStopLaser += handleOnStopFiring;
		}
		else
		{
			mustOscillate = true;
			StartCoroutine(coOscillate());
		}
	}


	void OnDisable()
	{
		if (isPlayerShip)
		{
			PlayerShipShoot.OnStartBullet -= handleOnStartFiring;
			PlayerShipShoot.OnStopBullet -= handleOnStopFiring;
			PlayerShipShoot.OnStartLaser -= handleOnStartFiring;
			PlayerShipShoot.OnStopLaser -= handleOnStopFiring;
		}
		else
		{
			StopAllCoroutines();
		}
	}


	void handleOnStartFiring()
	{
		mustOscillate = true;
		StopAllCoroutines();
		StartCoroutine(coOscillate());
	}


	void handleOnStopFiring()
	{
		mustOscillate = false;
	}


	IEnumerator coOscillate()
	{
		do
		{
			currDelta += angularSpeed * Time.deltaTime;

			//We allow rotation around angle zero (otherwise when rotating 1 degree below zero, eulerAngles.z doesnt return -1 but 359
			//if (a > 180)
			//	a -= 360;

			if (currDelta > amplitude / 2f)
			{
				currDelta = amplitude / 2f;
				angularSpeed *= -1;
			}
			else if (currDelta < -amplitude / 2f)
			{
				currDelta = -amplitude / 2f;
				angularSpeed *= -1;
			}
			transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, defaultAngle + currDelta);
			yield return null;
		} while (mustOscillate);

		transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, defaultAngle);
	}
}
