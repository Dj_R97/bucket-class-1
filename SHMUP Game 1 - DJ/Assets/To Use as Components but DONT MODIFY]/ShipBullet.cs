﻿using UnityEngine;
using System.Collections;
using System;

public class ShipBullet : BulletBase
{

	public void init(Vector3 pos, Vector3 rot, float spd, float dmg)
	{
		transform.eulerAngles = rot;
		rb.rotation = rot.z;
		rb.position = transform.position = pos;
		defaultSpeed = spd;
		defaultDamage = dmg;
	}


	protected override void onCollEnterCont(Collision2D col)
	{
		if (col.gameObject.layer == Alias.LAYER_ENEMIES)
		{
			col.gameObject.GetComponent<EnemyCollider>().onHitByBullet(defaultDamage);
			destroyMyself();
		}
	}

}
