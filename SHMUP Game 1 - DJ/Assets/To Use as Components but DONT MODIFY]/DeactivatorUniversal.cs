﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivatorUniversal : TriggerableObject
{
	[SerializeField]
	SpriteRenderer spriteRenderer;
	[SerializeField]
	Animator animator;
	[SerializeField]
	Rigidbody2D rigidbody2d;
	[SerializeField]
	Collider2D collider2d;
	[SerializeField]
	MonoBehaviour[] otherComponentsToDeactivate;


	void OnEnable()
	{
		foreach (MonoBehaviour comp in otherComponentsToDeactivate)
		{
			comp.enabled = false;
		}

		if (spriteRenderer != null)
			spriteRenderer.enabled = false;
		if (animator != null)
			animator.enabled = false;
		if (rigidbody2d != null)
			rigidbody2d.simulated = false;
		if (collider2d != null)
			collider2d.enabled = false;
	}

}
