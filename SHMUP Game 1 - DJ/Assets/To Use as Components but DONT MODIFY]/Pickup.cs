﻿using UnityEngine;
using System.Collections;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Pickup : MonoBehaviour
{
	[SerializeField]
	Rigidbody2D rb;
	[SerializeField]
	Animator animator;
	[SerializeField]
	Pickups type;
	[SerializeField]
	bool isCycling;
	[SerializeField]
	float cyclingDelay = 2.5f;
	//[SerializeField]
	Vector2 swingRadii = new Vector2(0.06f, 0.12f);
	//[SerializeField]
	float swingSpeed = 3f;
	[SerializeField]
	GameObject objToSpawn;

	public delegate void DelegatePickup(Pickups type);
	static public event DelegatePickup OnPickup;
	//public event System.Action OnPickupInstance;

	bool isPicked;
	Pickups[] cycleList; // = { Pickups.BulletA, Pickups.BulletB, Pickups.BulletC, Pickups.LaserA, Pickups.LaserB };
	int currCycleListIdx;


	void Awake()
	{
		isPicked = false;
		enabled = false;
	}


	void OnBecameVisible()
	{
		enabled = true;
	}


	void Start()
	{
		//build cycleList
		cycleList = new Pickups[PlayerShipShoot.Me.weapons.Length];
		for (int i = 0; i < cycleList.Length; i++)
		{
			Pickups weaponPkup;
			switch (PlayerShipShoot.Me.weapons[i].type)
			{
				case Weapons.BulletA:
					weaponPkup = Pickups.BulletA;
					break;

				case Weapons.BulletB:
					weaponPkup = Pickups.BulletB;
					break;

				case Weapons.BulletC:
					weaponPkup = Pickups.BulletC;
					break;

				case Weapons.LaserA:
					weaponPkup = Pickups.LaserA;
					break;

				case Weapons.LaserB:
					weaponPkup = Pickups.LaserB;
					break;

				default:
					weaponPkup = Pickups.BulletA;
					break;
			}
			cycleList[i] = weaponPkup;
		}

		updateVisual();

		StartCoroutine(coSwing());
		if (isCycling && type != Pickups.Bomb)
			StartCoroutine(coCycle());
	}


	private IEnumerator coCycle()
	{
		//init current index
		currCycleListIdx = (int)type;

		do
		{
			yield return new WaitForSeconds(cyclingDelay);
			//cycle type
			if (++currCycleListIdx == cycleList.Length)
				currCycleListIdx = 0;
			type = cycleList[currCycleListIdx];
			updateVisual();
		} while (!isPicked);
	}


	IEnumerator coSwing()
	{
		Vector2 basePos = transform.position;
		float a = 0f;

		do
		{
			a += Time.deltaTime * swingSpeed;
			float xOffset = Mathf.Sin(a * 2) * swingRadii.x;
			float yOffset = Mathf.Sin(a) * swingRadii.y;
			rb.position = transform.position = new Vector3(basePos.x + xOffset, basePos.y + yOffset, 0f);
			yield return null;
		} while (!isPicked);
	}


	void OnTriggerEnter2D(Collider2D otherColl)
	{
		if (otherColl.gameObject.layer == Alias.LAYER_SHIP)
		{
			gotPicked();
		}
	}


	private void gotPicked()
	{
		isPicked = true;

		if (OnPickup != null)
			OnPickup(type);
		//if (OnPickupInstance != null)
		//	OnPickupInstance();

		if (objToSpawn != null)
		{
			Instantiate(objToSpawn, transform.position, Quaternion.identity);
		}

		Destroy(gameObject);
	}


	void updateVisual()
	{
		animator.SetInteger("Type", (int)type);
		animator.SetBool("isCycle", isCycling);
	}


	/// <summary>
	/// Display special visual for each type of Pickup in the scene view (editor only)
	/// </summary>
	void OnDrawGizmos()
	{
		Color color;
		string label;
		int textSize;

		if (type == Pickups.Bomb)
		{
			//Gizmos.color = Color.red;
			//Gizmos.DrawWireSphere(rb.position, 0.7f);
			color = new Color(1f, 0f, 0f, 0.4f);
			label = "B";
			textSize = 22;
		}
		else if (type == Pickups.Health)
		{
			color = new Color(0f, 1f, 0.3f, 0.4f);
			label = "+";
			textSize = 22;
		}
		else
		{
			color = isCycling ? new Color(1f, 1f, 0f, 0.4f) : new Color(1f, 1f, 1f, 0.4f);
			label = type.ToString();
			label = "[" + label[0] + label[label.Length - 1] + "]";
			textSize = 12;
		}

#if UNITY_EDITOR
		Handles.color = color;
		Handles.DrawSolidDisc(transform.position, Vector3.back, 0.6f);

		//Display label
		GUIContent content = new GUIContent(label);

		GUIStyle style = new GUIStyle();
		style.fontSize = textSize;
		style.fontStyle = FontStyle.Bold;
		Vector2 contentSize = style.CalcSize(content);
		style.contentOffset = new Vector2(-contentSize.x * 0.45f, -contentSize.y * 0.6f);

		Handles.Label(transform.position, content, style);
#endif
	}
}


public enum Pickups
{
	BulletA = 0,
	BulletB = 1,
	BulletC = 2,
	LaserA = 3,
	LaserB = 4,
	Bomb = 10,
	Health = 15,
}
