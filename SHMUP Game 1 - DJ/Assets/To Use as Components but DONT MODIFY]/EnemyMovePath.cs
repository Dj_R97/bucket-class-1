﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyMovePath : EnemyMove
{
	[NonSerialized]
	public Spawner mySpawner;
	[SerializeField]
	TriggerableObject[] componentsToTriggerOnPhase2;

	int currTgtWpIndex;
	bool isPhase2Reached = false;
	Vector2 moveDir;
	bool isPostPath;


	public void init(Spawner spawner)
	{
		endCondition = BehaviorChangeConditions.NA;
		mySpawner = spawner;
		currTgtWpIndex = 1;
		isPostPath = false;

		//Positionning enemy at the first waypoint's coordinates
		if (mySpawner != null)
			rb.position = transform.position = mySpawner.wayPoints[0].position;

		//Orienting enemy in the direction of the path, if there are more than one waypoints
		//if (path.Length > 1)
		//{
		//	transform.LookAt(path[currTargetIndex]);
		//	rb.rotation = transform.eulerAngles.z;
		//}
	}


	//USE THAT METHOD INSTEAD OF Update() FOR CLASSES THAT DERIVE FROM EnemyMove!
	override protected void doOnUpdate()
	{
		if (!isPostPath)
		{
			if (mySpawner != null)
			{
				moveOnPath();
				checkPhase2();
			}
			else
			{
				goPostPath();
			}
		}
		else
		{
			movePostPath();
		}
	}


	void moveOnPath()
	{
		float move = speed * Time.deltaTime;

		//If there is still a waypoint as target, we check if we are close enough to it to consider we have reached it, and target the next index
		if (currTgtWpIndex < mySpawner.wayPoints.Length)
		{
			Vector2 v = rb.position - (Vector2)mySpawner.wayPoints[currTgtWpIndex].position;
			if (v.sqrMagnitude < move * move)
			{
				currTgtWpIndex++;
			}
		}

		Vector2 newPos;
		//Once the wp index has been potentially updated, we check if the target index is valid (i.e. if we are still in the path or have finished it)
		if (currTgtWpIndex < mySpawner.wayPoints.Length)
		{
			//If we are in the path we move towards the target wp
			newPos = Vector2.MoveTowards(rb.position, mySpawner.wayPoints[currTgtWpIndex].position, move);
			moveDir = newPos - rb.position;
			transform.position = rb.position = newPos;
		}
		else
		{
			//If the path is finished we activate the next EnemyMove behavior
			goPostPath();
		}
	}


	void checkPhase2()
	{
		//If we are beyond the waypoint to start a potential phase 2 of the enemy, we inform it (but only once)
		if (!isPhase2Reached && currTgtWpIndex > mySpawner.phase2WpIndex)
		{
			isPhase2Reached = true;
			triggerPhase2();
		}
	}


	private void triggerPhase2()
	{
		foreach (TriggerableObject obj in componentsToTriggerOnPhase2)
		{
			obj.triggerMe();
		}
	}


	void goPostPath()
	{
		isPostPath = true;

		//if no EnemyMove behavior has been specified in the trigger list, we keep the current script enabled and will move straight in the current direction and speed
		if (!hasNextMoveBehavior)
		{
			moveDir.Normalize(); //we normalize moveDir so that we can use it as a pure direction (vector length of 1) henceforth
		}

		//We still move the enemy during this frame
		movePostPath();

		//And we trigger the other objects
		triggerOthersAndFinish();
	}


	void movePostPath()
	{
		Vector2 move = moveDir * (speed * Time.deltaTime);
		transform.position = rb.position = (Vector2)transform.position + move;
	}

}
