﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollSpeedChanger : TriggerableObject
{
	[SerializeField]
	float scrollingSpeed;

	void OnEnable()
	{
		CamScroller.Me.setScrollSpeed(scrollingSpeed); 
	}
}
