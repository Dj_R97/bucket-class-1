﻿using UnityEngine;
using System.Collections;
using System;

public class SpawnProp : TriggeringObject
{
	public EnemyHealth healthManager;
	public Animator animator;

	//public event System.Action OnOpened;

	void Awake()
	{
		foreach (TriggerableObject trigObj in componentsToTrigger)
		{
			if (trigObj is Spawner) {
				(trigObj as Spawner).initFromSpawnProp(this);
			}
		}
	}


	void OnEnable()
	{
		animator.SetTrigger("goOpen");
	}


	public void onDoneOpening()
	{
		triggerOthersAndFinish();

		//if (OnOpened != null)
		//	OnOpened();
	}
}
