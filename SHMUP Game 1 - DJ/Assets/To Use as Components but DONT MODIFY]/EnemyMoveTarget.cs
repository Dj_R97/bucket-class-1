﻿using UnityEngine;
using System.Collections;

public class EnemyMoveTarget : EnemyMove
{
	[SerializeField]
	protected Transform target;


	override protected void doOnStart()
	{
		base.doOnStart();
		//if no target specified, it is the player's ship
		if (target == null)
		{
			PlayerShipMove psm = FindObjectOfType<PlayerShipMove>();
			if (psm != null)
				target = FindObjectOfType<PlayerShipMove>().transform;
		}
	}


	//USE THAT METHOD INSTEAD OF Update() FOR CLASSES THAT DERIVE FROM EnemyMove!
	override protected void doOnUpdate()
	{
		moveToTarget();
	}


	protected override void checkDistanceCondition()
	{
		if (target == null)
			return;

		float d = Vector2.SqrMagnitude(transform.position - target.position);
		if (d < value * value)
		{
			triggerOthersAndFinish();
		}
	}


	void moveToTarget()
	{
		//aim at the target
		//Vector2 v = target.position - transform.position;
		//v.Normalize();
		//Vector2 move = v * (speed * Time.deltaTime);
		//Vector2 newPos = new Vector2(transform.position.x + move.x, transform.position.y + move.y);
		//transform.position = rb.position = newPos;

		if (target != null)
		{
			Vector2 newPos = Vector2.MoveTowards(rb.position, target.position, speed * Time.deltaTime);
			transform.position = rb.position = newPos;
		}
	}

}
