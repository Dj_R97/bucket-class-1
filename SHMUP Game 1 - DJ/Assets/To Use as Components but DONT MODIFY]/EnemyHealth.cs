﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyHealth : TriggeringObject
{
	[SerializeField]
	EnemyInfo info;
	[SerializeField]
	private Transform toInstantiateOnDeath;
	[SerializeField]
	private float hitPoints = 20f;
	[SerializeField]
	private float dmgTakenOnShipHit = 50f;
	[SerializeField]
	private float dmgTakenOnMapHit = 2f;
	[SerializeField]
	Timer doomTimer;

	public delegate void DelegateEnemyType(Enemies e);
	static public event DelegateEnemyType OnKilledStatic;
	public event System.Action OnKilledInstance;

	Spawner mySpawner;


	void OnEnable()
	{
		if (doomTimer != null)
			doomTimer.OnTimesUp += handleOnTimesUp;
	}


	void OnDisable()
	{
		if (doomTimer != null)
			doomTimer.OnTimesUp -= handleOnTimesUp;
	}


	private void handleOnTimesUp()
	{
		//unregister from event
		doomTimer.OnTimesUp -= handleOnTimesUp;

		takeDamage(Mathf.Infinity, false);
	}


	public void register(Spawner spawner)
	{
		mySpawner = spawner;
	}


	public float getHps()
	{
		return hitPoints;
	}


	//Called by EnemyCollider when a player ship's bullet hits that enemy
	public void gotHitByWeapon(float dmg)
	{
		takeDamage(dmg);
	}


	public void gotHitByShip()
	{
		takeDamage(dmgTakenOnShipHit);
	}


	public void gotMapHit()
	{
		if (dmgTakenOnMapHit > 0)
			takeDamage(dmgTakenOnMapHit * Time.fixedDeltaTime, false);
	}


	void takeDamage(float dmg, bool mayBeAKill = true)
	{
		hitPoints -= dmg;

		if (hitPoints <= 0)
		{
			farewell();
			if (mayBeAKill)
			{
				if (OnKilledStatic != null)
					OnKilledStatic(info.type);

				if (OnKilledInstance != null)
					OnKilledInstance();
			}

			if (mySpawner != null)
				//Whether the enemy is killed by the player or by the environment, it's killed and we inform mySpawner accordingly
				mySpawner.doOnEnemyDestroyed(true, transform.position);
		}
	}


	void farewell()
	{
		triggerOthersAndFinish();

		//maybe instantiate a gameobject
		if (toInstantiateOnDeath != null)
			Instantiate(toInstantiateOnDeath, transform.position, Quaternion.identity);

		Destroy(gameObject);
	}
}
