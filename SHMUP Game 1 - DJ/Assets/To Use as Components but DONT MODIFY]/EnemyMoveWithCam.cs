﻿using UnityEngine;
using System.Collections;
using System;

public class EnemyMoveWithCam : EnemyMove
{
	Vector2 initialDeltaPos;
	Vector2 totalMove = Vector2.zero;


	protected override void doOnEnable()
	{
		initialDeltaPos = initialPos - (Vector2)Camera.main.transform.position;
	}


	protected override void doOnUpdate()
	{
		moveRelative();
	}


	override protected void checkDistanceCondition()
	{
		if (totalMove.x > value)
		{
			triggerOthersAndFinish();
		}
	}


	private void moveRelative()
	{
		totalMove.x += speed * Time.deltaTime;

		Vector2 newPos = (Vector2)Camera.main.transform.position + initialDeltaPos + totalMove;
		transform.position = rb.position = newPos;
	}
}
