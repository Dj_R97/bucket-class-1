﻿using UnityEngine;
using System.Collections;


public class EnemyInfo : MonoBehaviour
{
	public Enemies type;
	public float damageOnContact = 10f;
}


public enum Enemies
{
	EnemyA,
	EnemyB,
	EnemyC,
	EnemyD,
	EnemyE,
	EnemyF,
	EnemyG,
	EnemyH,
	EnemyI,
	EnemyJ,
	SpawnSiloA,
    SpawnSiloB
}
